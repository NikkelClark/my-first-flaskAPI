## This was my first API that I have created it's just a normal API with using
## json. At the time of writing this I have started working on a REST API.

from flask import Flask, jsonify, request, render_template

app = Flask(__name__)
stores = [
    {
    'name': 'My Wonderful Store',
    'items': [
        {'name': 'My Item', 'price': 15.99}
        ]
    }
]

@app.route('/') # 'http://www.google.com/'
def home():
    return render_template('index.html')

# POST /store data: {name:}
@app.route('/store', methods=['POST'])
def create_store():
    # When the browser send the request this is the data they sent
    request_data = request.get_json()
    # We are going to create the new store and fill in the request json data
    new_store = {
        'name' : request_data['name'],
        'items': []
    }
    # Once we created the new store we append it to the collection
    stores.append(new_store)
    # We then return the new store jsonifying it to make sure its a string
    return jsonify(new_store)

# GET  /store/<string:name>
@app.route('/store/<string:name>') # 'http://127.0.0.1:5000/store/some_name'
def get_store(name):
    # Iterate over store if store name matches return else return error message
    for store in stores:
        if store['name'] == name:
            return jsonify(store)
    return jsonify({'Error': 'Store doesn\'t exist'})

# GET /store
@app.route('/store')
def get_stores():
    return jsonify({'stores' : stores})

# POST /store/<string:name>/item {name:, price:}
@app.route('/store/<string:name>/item', methods=['POST'])
def create_item_in_store(name):
    request_data = request.get_json()
    for store in stores:
        if store['name'] == name:
            new_item = {
                'name': request_data['name'],
                'price': request_data['price']
            }
            store['items'].append(new_item)
            return jsonify(new_item)
    return jsonify({'Error':'Store not found'})

# GET /store/<string:name>/item
@app.route('/store/<string:name>/item')
def get_item_in_store(name):
    for store in stores:
        if store['name'] == name:
            return jsonify({'items':store['items']})
    return jsonify({'Error': 'We cannot find that item in the store!'})

app.run(port=5000)
