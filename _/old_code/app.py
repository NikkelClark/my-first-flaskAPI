from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt import JWT, jwt_required

from security import authenticate, identity

app = Flask(__name__) #Create our app
app.secret_key = 'ryan' # A private key
api = Api(app) #API

jwt = JWT(app, authenticate, identity)
#JWT creates a new endpoint called /auth
# It takes a username and password and send it to authetnicate
# If they match they return user and it become to user return
# A JWT token. It uses the JWT token to get the correct user from the
# identity function

items = []

class Item(Resource): #This class inherits from Recourse (from import)
    # Grab data out of the request we want
    parser = reqparse.RequestParser()
    parser.add_argument('price',
        type=float,
        required=True,
        help="This field cannot be left blank!"
    )

    @jwt_required()
    def get(self, name):
        # Lamba version
        item = next(filter(lambda x: x['name'] == name, items), None)
        # for item in items:
        #     if item['name'] == name:
        #         return item
        return {'item': item}, 200 if item else 404 #Not found

    def post(self, name):
        if next(filter(lambda x: x['name'] == name, items), None):
            return {'message': "An item with name '{}' already exists.".format(name)}, 400
            # 400 is bad request

        #Get the data from the request
        data = Item.parser.parse_args()
        #data = request.get_json()

        item = {'name': name, 'price': data['price']}
        items.append(item)
        return item, 201 #Created (202 is used then the creation is delayed)

    def delete(self, name):
        global items
        items = list(filter(lambda x: x['name'] != name, items))
        return {'message': 'Item deleted'}

    def put(self, name):
        #Get the data from the request
        data = Item.parser.parse_args()
        #data = request.get_json()

        item = next(filter(lambda x: x['name'] == name, items), None)
        if item is None: # If item doesn't exist then we add it
            item = {'name': name, 'price': data['price']}
            items.append(item)
            return item, 201 #Created a new item
        else: # If item already exist we update it
            item.update(data)
        return item

class ItemList(Resource):
    def get(self):
        return {'items': items}

# http://127.0.0.1:5000/student/name
api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')

app.run(port=5000, debug=True)
