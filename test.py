import sqlite3

# Create/connect with the data.db file
connection = sqlite3.connect('data.db')

cursor = connection.cursor()

# Crate a database in the database
create_table = "CREATE TABLE users (id int, username text, password text)"
cursor.execute(create_table)

# Insert a user into the database
user = (1, 'Ryan', 'Password')
insert_query = "INSERT INTO users values (?, ?, ?)"
cursor.execute(insert_query, user)

# Insert many users into the database
users = [
    (2, 'Chris', 'Password'),
    (3, 'Damla', 'Password')
]
cursor.executemany(insert_query, users)

# Retrive data from the database
select_query = "SELECT * FROM users"
for row in cursor.execute(select_query):
    print(row)

# Save changes
connection.commit()
# Closed the database connection
connection.close()
