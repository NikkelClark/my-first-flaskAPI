import os

from flask import Flask
from flask_restful import Api
from flask_jwt import JWT

from security import authenticate, identity
from resources.user import UserRegister # Importing from the package
from resources.item import Item, ItemList # Importing from the package
from resources.store import Store, StoreList # Import from the package

app = Flask(__name__) #Create our app
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL', 'sqlite:///data.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # Turns off Flask tracker only
app.secret_key = 'ryan' # A private key
api = Api(app) #API

jwt = JWT(app, authenticate, identity)
#JWT creates a new endpoint called /auth
# It takes a username and password and send it to authetnicate
# If they match they return user and it become to user return
# A JWT token. It uses the JWT token to get the correct user from the
# identity function

# http://127.0.0.1:5000/student/name
api.add_resource(Store, '/store/<string:name>')
api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(UserRegister, '/register')
api.add_resource(StoreList, '/stores')

# When we run a file with python it automatically calls it __main__
# by doing this if we add 'from app import ...' in another file it wouldn't
# run the app as __name__ wouldn't be equal too __main__
if __name__ == '__main__':
    from db import db
    db.init_app(app)
    app.run(port=5000, debug=True)
