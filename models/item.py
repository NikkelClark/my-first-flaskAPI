from db import db

class ItemModel(db.Model):
    # Using sqlalchemy
    __tablename__ = 'items'

    # Created variables using sqlalchemy for id, name and price.
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(precision=2))

    store_id = db.Column(db.Integer, db.ForeignKey('stores.id'))
    store = db.relationship('StoreModel')

    def __init__(self, name, price, store_id):
        self.name = name
        self.price = price
        self.store_id = store_id

    def json(self):
        return {'name': self.name, 'price': self.price}

    @classmethod
    def find_by_name(cls, name):
        # Can filter_by().filter_by() or filter_by(name=name, id=1)
        # to get the frist row add .first() to the end.
        return cls.query.filter_by(name=name).first()

    def save_to_db(self):
        # We can add our self since we are already an object and commit changes
        # This can be used for inserting or updating the database
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
