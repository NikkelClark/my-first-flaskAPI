from db import db

class StoreModel(db.Model):
    # Using sqlalchemy
    __tablename__ = 'stores'

    # Created variables using sqlalchemy for id, name and price.
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))

    # lazy is dynamic stop it from executing when the class is used
    # instead def json self.items.all() is was activates it as it a query builder
    items = db.relationship('ItemModel', lazy='dynamic')

    def __init__(self, name):
        self.name = name

    def json(self):
        return {'name': self.name, 'items': [item.json() for item in self.items.all()]}

    @classmethod
    def find_by_name(cls, name):
        # Can filter_by().filter_by() or filter_by(name=name, id=1)
        # to get the frist row add .first() to the end.
        return cls.query.filter_by(name=name).first()

    def save_to_db(self):
        # We can add our self since we are already an object and commit changes
        # This can be used for inserting or updating the database
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
