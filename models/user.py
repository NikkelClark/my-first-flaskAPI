import sqlite3
from db import db

class UserModel(db.Model):
    # Using sqlalchemy
    __tablename__ = 'users'

    # Created variables using sqlalchemy for id, username and password.
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    # Create user object
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # Since we don't user self anywhere inside the method we can
    # Instead use the class name
    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()
