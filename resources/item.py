from flask_restful import Resource, reqparse
from flask_jwt import jwt_required
from models.item import ItemModel

class Item(Resource): #This class inherits from Recourse (from import)
    # Grab data out of the request we want
    parser = reqparse.RequestParser()
    parser.add_argument('price',
        type=float,
        required=True,
        help="This field cannot be left blank!"
    )
    parser.add_argument('store_id',
        type=int,
        required=True,
        help="Every item needs a store id."
    )

    @jwt_required()
    def get(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            return item.json()
        return {'message': 'Item not found'}, 404

    def post(self, name):
        #if next(filter(lambda x: x['name'] == name, items), None):
        if ItemModel.find_by_name(name):
            return {'message': "An item with name '{}' already exists.".format(name)}, 400
            # 400 is bad request

        #Get the data from the request
        data = Item.parser.parse_args()
        # Again refactoring the code to work with the object oriented model
        # item = {'name': name, 'price': data['price']}
        item = ItemModel(name, data['price'], data['store_id'])

        try:
            # Again refactoring the object oriented additions
            #ItemModel.insert(item)
            item.save_to_db()
        except:
            return {'message': 'An error occured inserting the item'}, 500
        # 500 internal server error

        #items.append(item)
        return item.json(), 201 #Created (202 is used then the creation is delayed)

    def delete(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()

        return {'message': 'Item deleted'}

    def put(self, name):
        #Get the data from the request
        data = Item.parser.parse_args()

        item = ItemModel.find_by_name(name) # Return item object

        if item is None: # If item doesn't exist then we add it
            item = ItemModel(name, data['price'], data['store_id'])
        else: # If item already exist we update it
            item.price = data['price']

        item.save_to_db()

        return item.json()

class ItemList(Resource):
    def get(self):
        return {'items': [item.json() for item in ItemModel.query.all()]}
        #{'items': list(map(lambda x: x.json(), ItemModel.query.all()))}
