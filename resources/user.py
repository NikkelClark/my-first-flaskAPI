import sqlite3
from flask_restful import Resource, reqparse
from models.user import UserModel

class UserRegister(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('username',
        type=str,
        required=True,
        help="This field cannot be left blank!"
    )
    parser.add_argument('password',
        type=str,
        required=True,
        help="This field cannot be left blank!"
    )

    def post(self):
        data = UserRegister.parser.parse_args()

        # Validation
        if UserModel.find_by_username(data['username']):
            return {"message": "A user with that username already exists"}, 400

        # Because we are using a parser we know its always going to have user
        # and password so we can just unpack them.
        user = UserModel(**data)
        user.save_to_db()

        return {"message": "User created successfully."}, 201
