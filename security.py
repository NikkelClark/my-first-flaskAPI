from werkzeug.security import safe_str_cmp #Flask security import
from models.user import UserModel #Imprting from the package

# When a user is authenticating we get ensure their username and Password
# match. We use mapping to get the user account and ensure the Password
# maches.
def authenticate(username, password):
    #user = username_mapping.get(username, None)
    user = UserModel.find_by_username(username)
    if user and safe_str_cmp(user.password, password):
        return user

# When I user has already authenticated their request carried a payload
# which contains a payload with their user.id which we verify.
def identity(payload):
    user_id = payload['identity']
    #return userid_mapping.get(user_id, None)
    return UserModel.find_by_id(user_id)
